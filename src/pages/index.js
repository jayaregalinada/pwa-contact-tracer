import React from "react"

import SEO from "../components/skeleton/seo"
import Layout from "../components/skeleton/layout"

import "../scss/index.scss"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <img className="sampleImg" src="images/sample.png"></img>
  </Layout>
)

export default IndexPage
